#include <stdio.h>
#include <stdlib.h>
#include <string.h>


#define EXPECTED_ARGUMENTS 4
#define MODE_INDEX 1
#define SRC_INDEX 2
#define DST_INDEX 3
#define TEXT "textCopy"
#define BIN "binaryCopy"
#define SRC_MODE "r"
#define B_SRC_MODE "rb"
#define DST_MODE "w"
#define B_DST_MODE "wb"


//globals, used in a lot of functions...
FILE* src = NULL;
FILE* dst = NULL;


/*  The function get the path of the dst file,  and mode of the src file (because you need to check
	if you can open it -> file exists) and try to open the file
	input: dst path, src mode
	output: 0 -> file exists, 1 -> not*/
int exists(char* fname, char* fmode)
{
	FILE* f = fopen(fname, fmode);
	if (f != NULL)
	{
		fclose(f);
		return 1;
	}
	return 0;
}


/*  The function get the src path & mode, and the dst path & mode, and open them
	input: src path, src mode, dst path, dst mode
	output: none*/
void open_files(char* src_path, char* src_mode, char* dst_path, char* dst_mode)
{
	int num = 0;

	src = fopen(src_path, src_mode);
	if (!src)
	{
		printf("%s file does not exist.\n", src_path);
		exit(0);
	}

	// overwrite ??
	if (exists(dst_path, src_mode))
	{
		printf("Do you want to overwrite? 0 (no) / 1 (yes)");
		scanf("%d", &num);
		if (!num)
		{
			fclose(src);
			exit(0);
		}
	}


	dst = fopen(dst_path, dst_mode);
	if (!dst)
	{
		fclose(src);
		printf("Error!\n");
		exit(0);
	}
}


/*  The function copy text files
	input: none
	output: none*/
void copy_text_files()
{
	char ch = ' ';
	while ((ch = fgetc(src)) != EOF)
	{
		fputc(ch, dst);
	}
}


/*  The function copy binary files
	input: none
	output: none*/
void copy_bin_files()
{
	int size = 0;
	char* temp = 0;

	fseek(src, 0, SEEK_END);
	size = ftell(src);
	rewind(src);

	temp = (char*)malloc(size * sizeof(char));

	fread(temp, sizeof(char), size, src);
	fwrite(temp, sizeof(char), size, dst);

	free(temp);
}


int main(int argc, char* argv[])
{
	//valid number of argument
	if (argc != EXPECTED_ARGUMENTS)
	{
		printf("Invalid execution.");
		printf("Usage: copyFile.exe(textCopy | binaryCopy) < sourceFilename_filename > < destFilenameination_filename>");

		return 0;
	}
	
	//check text mode
	if (strcmp(argv[MODE_INDEX], TEXT) == 0)
	{
		open_files(argv[SRC_INDEX], SRC_MODE, argv[DST_INDEX], DST_MODE);
		copy_text_files();
	}

	//check binary mode
	else if (strcmp(argv[MODE_INDEX], BIN) == 0)
	{
		open_files(argv[SRC_INDEX], B_SRC_MODE, argv[DST_INDEX], B_DST_MODE);
		copy_bin_files();
	}


	else {
		printf("Invalid execution.\nUsage: copyFile.exe(textCopy | binaryCopy) < sourceFilename_filename > < destFilenameination_filename>\n");
		return 1;
	}

	printf("copy completed!");
	fclose(src);
	fclose(dst);
	getchar();
	return 0;
}
